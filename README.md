# smartSync

#### 介绍
4 Easier to synchronize data

#### 软件架构
暂无软件架构


#### 安装教程
暂无安装教程

#### 使用说明

1. composer require abo/smartsync
2. php artisan smartsync:make {table_name}      # table_name: xx_user
3. php artisan sync:{table_name}                # 自定义同步任务

#### 迭代说明

- v1.0.0  自动创建同步变,于 Logic 开放 新增/变更/失联 数据业务处理方法
- v1.0.1  syncTimePeriod() 开放 业务控制 返回某时间段内 同步数据
