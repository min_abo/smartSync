<?php

namespace Abo\Smartsync;

use Illuminate\Support\ServiceProvider;
class SmartsyncServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Abo\Smartsync\Console\MakeCommand',
    ];

    /** Bootstrap the application services @return void */
    public function boot()
    {
        if ($this->app->runningInConsole()) {

        }
    }

    /** Register the application services. */
    public function register()
    {
        $this->commands( $this->commands );
    }

}
