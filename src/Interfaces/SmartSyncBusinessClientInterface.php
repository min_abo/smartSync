<?php
/**
 * Description:
 * Abo 2019/4/27 10:17
 * Email: abo2013@foxmail.com
 */

namespace Abo\Smartsync\Interfaces;

interface SmartSyncBusinessClientInterface
{
    // 处理新增数据
    public function handleInsertSyncList();

    // 处理变更
    public function handleUpdateSyncList();

    // 处理 失联数据
    public function handleUnsetSyncList();

    // 同步数据 时间
    public function syncTimePeriod();
}