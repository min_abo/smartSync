<?php
/**
 * Function:
 * Description:
 * Abo 2019/3/17 12:58
 * Email: abo2013@foxmail.com
 */
namespace Abo\Smartsync\Config;

class ColumnSql
{
    const PRIMARY_KEY = " int(11) unsigned NOT NULL AUTO_INCREMENT";

    const INT = " int(11) unsigned NOT NULL DEFAULT 0 COMMENT ''";

    const VARCHAR = " varchar(255) NOT NULL DEFAULT '' COMMENT ''";
    const TEXT = " text COMMENT ''";
    const DECIMAL = " decimal(10,2) unsigned DEFAULT '0.00' COMMENT ''";

    const CU_TIME = "`updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',`created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间'";

    public $columnItem = [];

    /**
     * 数字类型数据 => sql数组
     * @param $data
     * @return array
     */
    public function numeric2Sql( $key, $data )
    {
        if ( is_int( $data ) ) {
            $this->columnItem[] = $key . ColumnSql::INT;
        }else{ //if ( is_float( $data ) ) {
            $this->columnItem[] = $key . ColumnSql::DECIMAL;
        }

        return $this->columnItem;
    }

    public function mixData2Sql( $mixData )
    {
        if ( is_numeric( $data ) ) { // 非数字类型处理
            $data = isStringTypeNum( $stringData );
        }

    }

    /**
     * 是否为字符串类型 数字
     * @param $stringData
     * @return bool|float|int
     */
    protected function isStringTypeNum( $stringData )
    {
        $ret2Return = false;

        if ( $stringData == intval( $stringData ) ) {
            $ret2Return = intval( $stringData );
        }

        if ( $stringData == floatval( $stringData ) ) {
            $ret2Return = floatval( $stringData );
        }

        if ( $stringData == floor( $stringData ) ) {
            $ret2Return = floor( $stringData );
        }

        return $ret2Return;
    }
}