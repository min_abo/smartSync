<?php
return [

    'primary_key' => ":PRIMARY_KEY int(11) unsigned NOT NULL AUTO_INCREMENT",

    'varchar' => ":VARCHAR varchar(20) NOT NULL DEFAULT '' COMMENT '日期'",
    'text' => ":TEXT text COMMENT '素材url'",
    'decimal' => ":DECIMAL decimal(10,2) unsigned DEFAULT '0.00' COMMENT '消耗'",

    'cu_time' => "`updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间'",
];