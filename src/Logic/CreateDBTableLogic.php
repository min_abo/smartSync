<?php
/**
 * Function: 根据返回集合,组织 数据表sql语句
 * Description:
 * Abo 2019/3/17 11:22
 * Email: abo2013@foxmail.com
 */

namespace Abo\Smartsync\Logic;


use Abo\Smartsync\Config\ColumnSql;
use Illuminate\Support\Facades\DB;

class CreateDBTableLogic
{
    protected $tableName,$comment;
    protected $tablePrimaryId = 'id';

    /** @throws \Exception */
    public function __construct( string $tableName, string $tablePrimaryId = 'id', string $comment = '' )
    {
        if ( !$tableName ) {
            throw new \Exception( 'Invalid synchronize tableName', 500 );
        }
        $this->tableName = $tableName;
        $this->tablePrimaryId = $tablePrimaryId;

        if ( $comment ) {
            $this->comment = $comment;
        }
    }

    /**
     * 根据同步数据 创建表
     * @param array $syncData 单元数组,开始会比较简一化,后面需要做全数组兼容( 提取特征 )
     */
    public function createSyncTableByData( array $syncData = [] )
    {
        $columnItems = $this->checkTypeAddTableItem( $syncData );
        $createTableSql = $this->constructDBTableSql( $columnItems );

        return DB::statement( $createTableSql );
    }

    /**
     * 检查数据类型,生成数据表字段
     * @param array $syncData
     * @return array
     */
    protected function checkTypeAddTableItem( array $syncData = [] )
    {
        if ( !$syncData ) { return []; }
        $ColumnSql = new ColumnSql();

        foreach ( $syncData as $k2Data => $v2Data ){
            if ( $k2Data == $this->tablePrimaryId ) {
                $ColumnSql->columnItem[] = $this->tablePrimaryId . ColumnSql::PRIMARY_KEY;
                continue;
            }

            if ( is_numeric( $v2Data ) ){
                $ColumnSql->numeric2Sql( $k2Data, $v2Data );
                continue;
            }elseif ( is_string( $v2Data ) ) {
                $temStrLen = mb_strlen( $v2Data );
                if ( 200 > $temStrLen ) {
                    $ColumnSql->columnItem[] = $k2Data . ColumnSql::VARCHAR;
                }else{
                    $ColumnSql->columnItem[] = $k2Data . ColumnSql::TEXT;
                }
            }else{
                throw new \Exception( 'unknow data type:'. $k2Data . ' : ' . $v2Data, 500 );
            }
        }

        return $ColumnSql->columnItem;
    }

    /**
     * 构建完整表语句
     * @param array $columnItems
     * @return string
     */
    protected function constructDBTableSql( array $columnItems = [] )
    {
        if ( !$columnItems ) { return ''; }
        $columnItems[] = ColumnSql::CU_TIME;

        $baseSql = "CREATE TABLE IF NOT EXISTS `{$this->tableName}` ( "
            .implode(',', $columnItems)
            .",`md5_origin` varchar(32) DEFAULT '' COMMENT '原有md5加密'
              ,`md5_new` varchar(32) DEFAULT '' COMMENT '新的md5加密'
              ,`update_batch` varchar(32) DEFAULT '' COMMENT '更新批次'
              ,`remove_time`  int(11) unsigned NOT NULL DEFAULT 0 COMMENT '移除时间'"
            .",PRIMARY KEY (`{$this->tablePrimaryId}`) ) ENGINE=InnoDB AUTO_INCREMENT=1 COMMENT='{$this->comment}';";

        return $baseSql;
    }
}