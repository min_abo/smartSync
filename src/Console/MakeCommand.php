<?php

namespace Abo\Smartsync\Console;

use Abo\Generalutil\Utils\StringUtil;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeCommand extends GeneratorCommand
{
    /** The name and signature of the console command. @var string */
    protected $signature = 'smartsync:make {name}';

    protected $name = 'smartsync:make';

    /** The console command description. @var string */
    protected $description = 'create smartsync logic';

    /** Execute the console command. @return mixed */
    public function handle()
    {
        parent::handle(); // 创建文件
        $this->createClientCommands();
    }

    /** 获取输入类名 @return mixed */
    protected function getNameInput()
    {
        $nameInput = trim( $this->argument('name') );
        if ( !$nameInput ) { throw new \Exception( 'Unvalid input name', 500 ); }

        $StringUtil = new StringUtil();
        $name2Return = ucfirst( $StringUtil->camelize( $nameInput ) );

        return 'Sync'.$name2Return.'Logic';
    }
    
    /** 获取输入类名 @return mixed */
    protected function getLogicName()
    {
        $inputClassName = trim( $this->argument( 'name' ) );
        if ( !$inputClassName ) {
            $this->error('ClassName does not vaild !');
        }

        $className = strtolower( preg_replace( '/([a-z])([A-Z])/', "$1" . '_' . "$2", $inputClassName ) );
        return [ $className, $inputClassName ];
    }

    /**
     * stub 模板文件中替换 类名
     *
     * @param string $stub
     * @param string $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        list( $baseClassName, $inputName ) = $this->getLogicName();

        return str_replace(
            [
                'DummyClass',
                'DummyTable',
            ],
            [
                $baseClassName,
                $inputName,
            ],
            $stub
        );
    }

    /** 获取类的默认命名空间 @param string $rootNamespace @return string */
    protected function getDefaultNamespace($rootNamespace)
    {
        $directory = app_path( 'Logic\SmartSyncLogic' );

        $namespace = ucfirst(basename($directory));
        return $rootNamespace."\\$namespace";
    }

    /** 获取命令参数 @return array */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the synchronize data.'],
        ];
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

    /** Get the stub file for the generator @return string */
    protected function getStub()
    {
        return __DIR__.'/stubs/SmartSyncLogic.stub';
    }

    protected function createClientCommands()
    {
        $logicName = rtrim( $this->getNameInput(), 'SyncLogic' );
        $tableName = trim( $this->argument('name') );

        $filePath = app_path( 'Console/Commands/'.$logicName.'.php' );

        $cdBasePathCommand = 'cd '.base_path();
        shell_exec( $cdBasePathCommand );
        $createClientCommand = 'php artisan make:command '.$logicName.' --command=sync:'.$tableName;
        shell_exec( $createClientCommand );


        $fileContent = file_get_contents( $filePath );
        $fileContent = str_replace(
            'public function handle()
    {', 'public function handle()
    {
        return ( new \App\Logic\SmartSyncLogic\\'.$logicName.'Logic() )->syncData();',
            $fileContent
        );

        return file_put_contents( $filePath, $fileContent );
    }
}
